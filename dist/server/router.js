"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var client_1 = require("./controllers/client");
var user_1 = require("./controllers/user");
function setRoutes(app) {
    var router = express.Router();
    var clientCtrl = new client_1.default();
    var userCtrl = new user_1.default();
    console.log("comming");
    router.route('/login').post(userCtrl.login);
    router.route('/signup').post(userCtrl.signup);
    // router.use( (req, res, next) => {
    //     var token = req.body.token || req.params.token || req.headers['token'];
    //     if (token) {
    //         // verifies secret and checks exp
    //         jwt.verify(token, app.get('secret'), function(err, decoded) {			
    //             if (err) {
    //                 return res.json({ success: false, message: 'Failed to authenticate token.' });		
    //             } else {
    //                 req.decoded = decoded;	
    //                 next();
    //             }
    //         });
    //     } else {
    //         return res.status(403).send({ 
    //             success: false, 
    //             message: 'No token provided.'
    //         });
    //     }
    // });
    console.log("clients");
    router.route('/clients').get(clientCtrl.getAll);
    router.route('/clients/count').get(clientCtrl.count);
    router.route('/client').post(clientCtrl.insert);
    router.route('/clients/:id').get(clientCtrl.get);
    router.route('/clients/:id').put(clientCtrl.update);
    router.route('/clients/:id').delete(clientCtrl.delete);
    app.use('/api', router);
}
exports.default = setRoutes;
//# sourceMappingURL=router.js.map