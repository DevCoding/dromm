"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var dotenv = require("dotenv");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var mongoose = require("mongoose");
var path = require("path");
var cors = require("cors");
var config_1 = require("./config");
var router_1 = require("./router");
var app = express();
exports.app = app;
dotenv.load({ path: '.env' });
app.set('port', (process.env.PORT || 3000));
app.set('secret', config_1.default.secret);
app.use('/', express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
    origin: 'http://localhost:4200'
}));
app.use(morgan('dev'));
if (process.env.NODE_ENV === 'test') {
    mongoose.connect(config_1.default.mongodb_uri);
}
else {
    mongoose.connect(config_1.default.mongodb_uri, {
        server: {
            socketOptions: {
                socketTimeoutMS: 10000,
                connectionTimeout: 10000
            }
        }
    });
}
var db = mongoose.connection;
mongoose.Promise = global.Promise;
mongoose.connection.on('error', function (err) { });
db.on('error', console.error.bind(console, 'connection error:'));
db.on('open', function () {
    console.log('Connected to MongoDB');
    router_1.default(app);
    app.get('/*', function (req, res) {
        res.sendFile(path.join(__dirname, '../public/index.html'));
    });
    if (!module.parent) {
        app.listen(app.get('port'), function () {
            console.log('Angular Full Stack listening on port ' + app.get('port'));
        });
    }
});
//# sourceMappingURL=app.js.map