"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var clientSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    phoneNumber: String,
    email: String,
    gender: {
        type: String,
        enum: ['M', 'F', 'O']
    },
    dateofBirth: Date,
    address: String,
    service: String,
    userId: String,
    bloodPressures: [{ date: Date, systolic: Number, diastolic: Number }],
    weights: [{ date: Date, weight: Number }],
    temperatures: [{ date: Date, temperature: Number }]
});
var Client = mongoose.model('Client', clientSchema);
exports.default = Client;
//# sourceMappingURL=client.js.map