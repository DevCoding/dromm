
import * as express from 'express';
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as mongoose from 'mongoose';
import * as path from 'path';
import * as cors from 'cors';
import Config from './config';
import setRoutes from './router';

const app = express();
dotenv.load({ path: '.env' });
app.set('port', (process.env.PORT || 3000));
app.set('secret', Config.secret);
app.use('/', express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin : 'http://localhost:4200'
}));
app.use(morgan('dev'));
if (process.env.NODE_ENV === 'test') {
  mongoose.connect(Config.mongodb_uri);
} else {
  mongoose.connect(
    Config.mongodb_uri, {
      server: {
        socketOptions: {
          socketTimeoutMS: 10000,
          connectionTimeout: 10000
        }
      }
    });
}

const db = mongoose.connection;
(<any>mongoose).Promise = global.Promise;

mongoose.connection.on('error', function(err){});

db.on('error', console.error.bind(console, 'connection error:'));
db.on('open', () => {
  console.log('Connected to MongoDB');

  setRoutes(app);

  app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../public/index.html'));
  });

  if (!module.parent) {
    app.listen(app.get('port'), () => {
      console.log('Angular Full Stack listening on port ' + app.get('port'));
    });
  }

});

export { app };
