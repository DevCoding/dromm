import * as mongoose from 'mongoose';

const clientSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  phoneNumber: String,
  email: String,
  gender: {
    type: String,
    enum: ['M', 'F', 'O']
  },
  dateofBirth: Date,
  address: String,
  service: String,
  userId: String,
  bloodPressures: [{date: Date, systolic: Number, diastolic: Number}],
  weights: [{date: Date, weight: Number}],
  temperatures: [{date: Date, temperature: Number}]
});

const Client = mongoose.model('Client', clientSchema);

export default Client;