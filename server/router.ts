import * as express from 'express';
import ClientCtrl from './controllers/client';
import UserCtrl from './controllers/user';
import * as jwt from 'jsonwebtoken';

export default function setRoutes(app) {

    const router = express.Router();

    const clientCtrl = new ClientCtrl();
    const userCtrl = new UserCtrl();
    console.log("comming");
    router.route('/login').post(userCtrl.login);
    router.route('/signup').post(userCtrl.signup);

    // router.use( (req, res, next) => {

    //     var token = req.body.token || req.params.token || req.headers['token'];

    //     if (token) {
    //         // verifies secret and checks exp
    //         jwt.verify(token, app.get('secret'), function(err, decoded) {			
    //             if (err) {
    //                 return res.json({ success: false, message: 'Failed to authenticate token.' });		
    //             } else {
    //                 req.decoded = decoded;	
    //                 next();
    //             }
    //         });
    //     } else {
    //         return res.status(403).send({ 
    //             success: false, 
    //             message: 'No token provided.'
    //         });
    //     }
    // });
    console.log("clients");
    router.route('/clients').get(clientCtrl.getAll);
    router.route('/clients/count').get(clientCtrl.count);
    router.route('/client').post(clientCtrl.insert);
    router.route('/clients/:id').get(clientCtrl.get);
    router.route('/clients/:id').put(clientCtrl.update);
    router.route('/clients/:id').delete(clientCtrl.delete);

    app.use('/api', router);
}