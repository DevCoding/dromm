import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { User } from '../objects/user'; 
@Injectable()
export class AuthService {
  public headers: Headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  public options: RequestOptions = new RequestOptions({ headers: this.headers });
  loggedIn = false;
  isAdmin = false;
  currentUser : User = new User();
  jwtHelper: JwtHelper = new JwtHelper();


  constructor(    
    private http: Http,
    private router: Router
  ) 
  {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedUser = this.decodeUserFromToken(token);
      this.setCurrentUser(decodedUser);
    }
  }

  login(credentail) : Observable<any>{
    console.log(JSON.stringify(credentail).trim());
    return this.http.post('/api/login', JSON.stringify(credentail), this.options).map(res => res.json()).map(
      res => {
        localStorage.setItem('token', res.token);
        const decodedUser = this.decodeUserFromToken(res.token);
        console.log(res.token);
        console.log(decodedUser);
        this.setCurrentUser(decodedUser);
        return this.loggedIn;
      }
    );
  }
  signup(user) : Observable<any>{
    console.log(user);
    return this.http.post('/api/signup', JSON.stringify(user).trim(), this.options).map(res => res.json()).map(
      res => {
        localStorage.setItem('token', res.token);
        const decodedUser = this.decodeUserFromToken(res.token);
        console.log(res.token);
        console.log(decodedUser);
        this.setCurrentUser(decodedUser);
        return this.loggedIn;
      }
    );
  }
  logout() {
    localStorage.removeItem('token');
    this.loggedIn = false;
    this.isAdmin = false;
    this.currentUser = new User();
    this.router.navigate(['/']);
  }
  decodeUserFromToken(token) {
    return this.jwtHelper.decodeToken(token).user;
  }
  setCurrentUser(decodedUser) {
    this.loggedIn = true;
    this.currentUser._id = decodedUser._id;
    this.currentUser.firstName = decodedUser.firstName;
    this.currentUser.lastName = decodedUser.lastName;
    this.currentUser.email = decodedUser.email;
    this.currentUser.role = decodedUser.role;
    decodedUser.role === 'admin' ? this.isAdmin = true : this.isAdmin = false;
    delete decodedUser.role;
  }
}
