import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
@Injectable()
export class ClientService{
    public headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
    public options = new RequestOptions({ headers: this.headers });
    constructor(
        private http: Http
    ) 
    { 
    }
    createAuthenticationHeaders() {
        const token = localStorage.getItem('token');
        this.options = new RequestOptions({
            headers : new Headers({
            'token' : token,
            'Content-Type': 'application/json', 
            'charset': 'UTF-8'
            })
        });  
    }
    getAllClient(){
        //this.createAuthenticationHeaders();
        console.log(this.options);
        return this.http.get('/api/clients', this.options).map(res => res.json());
    }
    saveClient(client) {
        //this.createAuthenticationHeaders();
        console.log(client);
        console.log(this.options);
        return this.http.post('/api/client', JSON.stringify(client).trim(), this.options).map(res => res.json());
        //return this.http.get('/api/clients', this.options).map(res => res.json());
    }
    removeClient(client) {
        return this.http.delete(`/api/clients/${client._id}`, this.options);
    }
    editClient(client) {
        return this.http.put(`/api/clients/${client._id}`, JSON.stringify(client), this.options);
    }
}
