import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animation';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { User } from '../objects/user';
import { AuthService } from '../services/auth.service';
import { error } from 'selenium-webdriver';
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.scss'
  ],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  
  constructor(
    public router: Router,
    public fb: FormBuilder,
    private auth: AuthService
    ) { }

  ngOnInit() {
  
    this.form = this.fb.group({
      firstName: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
      lastName: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])]
    });
    $('.form').find('input, textarea').on('keyup blur focus', function (e) {
      
      var $this = $(this),
          label = $this.prev('label');
    
        if (e.type === 'keyup') {
          if ($this.val() === '') {
              label.removeClass('active highlight');
            } else {
              label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
          if( $this.val() === '' ) {
            label.removeClass('active highlight'); 
          } else {
            label.removeClass('highlight');   
          }   
        } else if (e.type === 'focus') {
          
          if( $this.val() === '' ) {
            label.removeClass('highlight'); 
          } 
          else if( $this.val() !== '' ) {
            label.addClass('highlight');
          }
        }
    
    });
    
    $('.tab a').on('click', function (e) {
      
      e.preventDefault();
      
      $(this).parent().addClass('active');
      $(this).parent().siblings().removeClass('active');
      
      var target = $(this).attr('href');
    
      $('.tab-content > div').not(target).hide();
      
      $(target).fadeIn(600);
      
    });
  }
  onSignin() {
    console.log(this.form.value);
    this.auth.login(this.form.value).subscribe(
      res => {
        alert("success");
        this.router.navigate(['/dashboard']);
      },
      error => {
        alert("error");
      }
    );
    // localStorage.setItem('isLoggedin', 'true');
    //this.router.navigate(['/dashboard']);
  }
  onSignup(){
    
    this.auth.signup(this.form.value).subscribe(
      res => {
        alert("success");
        this.router.navigate(['/dashboard']);
      },
      error => {
        alert("error");
      }
    );
  }
  onClose(){
    this.router.navigate(['/']);
  }
}
