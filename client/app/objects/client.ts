export class Client {
    _id: string;
    no: number;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    gender: string;
    dateofBirth: string;
    address: string;
    service: string;
    userId: string;
    bloodPressures: BloodPressure[];
    weights: Weight[];
    temperatures: Temperature[];

    constructor()
    {

    }
    setValue(
        _id: string,
        firstName: string,
        lastName: string,
        phoneNumber: string,
        email: string,
        gender: string,
        dateofBirth: string,
        address: string,
        service: string,
        userId: string,
        bloodPressures: BloodPressure[],
        weights: Weight[],
        temperatures: Temperature[]
    ) 
    {
        this._id = _id;
        this.no = 0;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.gender = gender;
        this.dateofBirth = dateofBirth;
        this.address = address;
        this.service = service;
        this.userId = userId;
        this.bloodPressures = bloodPressures;
        this.weights = weights;
        this.temperatures = temperatures;
    }
    setJsonValue(element) {
        this._id = element._id;
        this.no = 0;
        this.firstName = element.firstName;
        this.lastName = element.lastName;
        this.phoneNumber = element.phoneNumber;
        this.email = element.email;
        this.gender = element.gender;
        this.dateofBirth = element.dateofBirth;
        this.address = element.address;
        this.service = element.service;
        this.userId = element.userId;
        this.bloodPressures = element.bloodPressures;
        this.weights = element.weights;
        this.temperatures = element.temperatures;
    }
    setNo(no: number) {
        this.no = no;
    }
}
export class BloodPressure {
    date: Date;
    systolic: number;
    diastolic: number;
}
export class Weight {
    date: Date;
    weight: number;
}
export class Temperature {
    date: Date;
    temperature: number;
}
