export class User {
    _id: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string; 
    role: string;
    constructor(){
        this._id = "";
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.password = "";
        this.role = "";
    }
}
