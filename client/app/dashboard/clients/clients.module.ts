import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'; 
import { ClientsComponent } from './clients.component';
import { ClientsRoutingModule } from './clients-routing.module';

import { ClientService } from '../../services/client.service';
import { EditClientComponent } from './edit-client/edit-client.component';

import { SharedModule } from '../shared/shared.module';
import { AuthService } from '../../services/auth.service';
@NgModule({
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule,
    SharedModule 
  ],
  declarations: [ClientsComponent, EditClientComponent],
  providers: [
    ClientService,
    AuthService
  ],
  entryComponents: [
    EditClientComponent
  ]
})
export class ClientsModule { }
