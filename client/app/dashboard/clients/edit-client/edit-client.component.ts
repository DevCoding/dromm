import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../../services/auth.service';
@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent {

  public form : FormGroup;
  public dialogTitle: string = "Create Client";
  constructor(
    public dialogRef: MatDialogRef<EditClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb: FormBuilder,
    private auth: AuthService
  ) 
  {
    this.form = this.fb.group({
      firstName: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
      lastName: [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      phoneNumber : [null, Validators.compose([Validators.required])],
      gender: [null, Validators.compose([Validators.required])],
      dateofBirth: [null, Validators.compose([Validators.required])],
      address: [null, Validators.compose([Validators.required])],
      service: [null, Validators.compose([Validators.required])],

      _id: ['', null],
      no: [0, null],
      userId: [this.auth.currentUser._id, null],
      bloodPressures: [[], null],
      weights: [[], null],
      temperatures: [[], null]
    });

    // this.form.setValue({
    //   firstName: "Smith",
    //   lastName: "Larsson",
    //   phoneNumber: "1912345",
    //   email: "smith@gmail.com",
    //   gender: "M",
    //   dateofBirth: new Date(),
    //   address: "AAAA",
    //   service: "BBBB",
    //   userId: this.auth.currentUser._id,
    // });
  }
  setFormValue(value) {
    this.form.setValue(value);
  }
  fnameError() {
    return this.form.controls['firstName'].hasError('required') ? 'You must enter a value' :
    this.form.controls['firstName'].hasError('maxlength') ? 'Not a valid' : '';
  }
  lnameError() {
    return this.form.controls['lastName'].hasError('required') ? 'You must enter a value' :
    this.form.controls['lastName'].hasError('maxlength') ? 'Not a valid' : '';
  }
  emailError() {
    return this.form.controls['email'].hasError('required') ? 'You must enter a value' :
    this.form.controls['email'].hasError('email') ? 'Not a valid email' : '';
  }
  phoneNumberError() {
    return this.form.controls['phoneNumber'].hasError('required') ? 'You must enter a value' : '';
  }
  genderError() {
    return this.form.controls['gender'].hasError('required') ? 'You must enter a value' : '';
  }
  dateofBirthError() {
    return this.form.controls['dateofBirth'].hasError('required') ? 'You must enter a value' : '';
  }
  addressError() {
    return this.form.controls['address'].hasError('required') ? 'You must enter a value' : '';
  }
  serviceError() {
    return this.form.controls['service'].hasError('required') ? 'You must enter a value' : '';
  }
}
