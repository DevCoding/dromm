import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Client } from '../../objects/client';
import { ClientService } from '../../services/client.service';
import { AuthService } from '../../services/auth.service';
import { EditClientComponent } from './edit-client/edit-client.component';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { error } from 'util';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
    displayedColumns = ['no', 'firstName', 'lastName', 'phoneNumber', 'email', 'gender', 'dateofBirth', 'address', 
    'service','measuring' , 'actions'];
    ELEMENT_DATA : Client[] = [];
    dataSource = new MatTableDataSource<Client>(this.ELEMENT_DATA);

    @ViewChild(MatPaginator) paginator: MatPaginator;
    constructor( 
        private clientService: ClientService ,
        public dialog: MatDialog,
        private authService: AuthService
    ) 
    {
    }
    ngOnInit() {

    }
    ngAfterViewInit() {
        this.getList();
        this.dataSource.paginator = this.paginator;
        
    }
    getList() {
        this.clientService.getAllClient().subscribe(
            res => {          
                var array : Client[] = [];
                var index = 1;
                res.forEach(element => {
                    var client = new Client();
                    client.setJsonValue(element);
                    client.setNo(index);
                    array.push(client);
                    index = index + 1;
                });
                this.ELEMENT_DATA = array;
                console.log(array);
                this.dataSource.data = this.ELEMENT_DATA;
            }
        );
    }

    onAdd() {
        let dialogRef = this.dialog.open(EditClientComponent, {
            panelClass : 'col-md-5'
          });
        dialogRef.componentInstance.dialogTitle = "Create Client";  
        dialogRef.afterClosed().subscribe(result => {
            
            if (result == true) {
                var value = JSON.parse( JSON.stringify( dialogRef.componentInstance.form.value) );
                delete value._id;
                delete value.no;
                this.clientService.saveClient(value).subscribe(
                    res => {
                        this.getList();
                    }
                );
            }
        });
    }
    onRemove(number) {
        this.clientService.removeClient(this.ELEMENT_DATA[number - 1]).subscribe(
            res => {
                this.getList();
            }, 
            error => {
                alert(error);
            }
        );
    }
    onEdit(number) {
        let dialogRef = this.dialog.open(EditClientComponent, {
            panelClass : 'col-md-5'
          });
        dialogRef.componentInstance.dialogTitle = "Edit Client";  
        //dialogRef.componentInstance.form.setValue(this.ELEMENT_DATA[number - 1]);
        dialogRef.componentInstance.setFormValue(this.ELEMENT_DATA[number - 1]);
        dialogRef.afterClosed().subscribe(result => {
            
            if (result == true) {
                var value = JSON.parse( JSON.stringify( dialogRef.componentInstance.form.value) );
                delete value.no;
                this.clientService.editClient(value).subscribe(
                    res => {
                        this.getList();
                    }
                );
            }
        });
    }
}
