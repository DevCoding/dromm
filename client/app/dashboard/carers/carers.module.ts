import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarersComponent } from './carers.component';
import { CarersRoutingModule } from './carers-routing.module';
@NgModule({
  imports: [
    CommonModule,
    CarersRoutingModule
  ],
  declarations: [CarersComponent]
})
export class CarersModule { }
