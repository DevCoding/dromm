import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
      public router: Router
  ) { }

  ngOnInit() {

  }
  onClients() {
    this.router.navigate(['/dashboard/clients']);
  }

  onCarers() {
    this.router.navigate(['/dashboard/carers']);
  }

  onPlanning() {
    this.router.navigate(['/dashboard/planning']);
  }

  onReports() {
    this.router.navigate(['/dashboard/reports']);
  }

  onBilling() {
    this.router.navigate(['/dashboard/billing']);
  }

  onAccount() {
    this.router.navigate(['/dashboard/account']);
  }

  onLogout() {
   // this.router.navigate(['/dashboard/clients']);
  }
}
